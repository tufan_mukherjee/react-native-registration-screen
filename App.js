/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {createStackNavigator, createAppContainer} from 'react-navigation';

import RegisterScreen from "./app/screen/Register"
import LoginScreen from "./app/screen/LoginScreen"

import { flipX } from 'react-navigation-transitions';
const RootStack = createStackNavigator(
  {
    RegisterScreen: {
      screen: RegisterScreen,
    },
    LoginScreen: {
      screen: LoginScreen,
    },
  },
  {
    initialRouteName: 'RegisterScreen',
    transitionConfig: () => flipX(),

  }
);

const AppContainer = createAppContainer(RootStack);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}
