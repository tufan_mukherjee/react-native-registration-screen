import React, { Component } from "react";
import { Button, View, Text } from 'react-native';
import {StyleSheet} from "react-native";
export default class LoginScreen extends Component {
    render() {
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Text style={styles.headerText}>Login Screen</Text>
          {/* <Button
            title="Go to Details... again"
            onPress={() => this.props.navigation.push('RegisterScreen')}
          />
          <Button
            title="Go to Home"
            onPress={() => this.props.navigation.navigate('RegisterScreen')}
          /> */}
          <Button
            title="Go back"
            onPress={() => this.props.navigation.goBack()}
          />
        </View>
      );
    }
  }
  const styles = StyleSheet.create({
    headerText:{
      fontSize :24,
      color:'black',
      marginBottom: 10,
    }
  });