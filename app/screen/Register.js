
import React, { Component } from "react";
import {StyleSheet,Text,View,TextInput,TouchableHighlight,ScrollView} from "react-native";
import * as constants from "../utils/const";
import Dialog, { DialogFooter, DialogButton, DialogContent } from 'react-native-popup-dialog';

export default class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      mobileNumber: "",
      email: "",
      password: "",
      emailValid: false,
      passwordValid: false,
      mobileValid: false,
      firstNameValid: false,
      lastNameValid: false,
      isWrongEmail: false,
      isValidPassword: false,
      isWrongMobile: false,
      isNotValidFirstName: false,
      isNotValidLastName: false,
      modalVisible: false
    };
  }

  onClickListener = viewId => {
    this.setModalVisible(true);
  };
  onChangeEmail = value => {
    const reg = constants.emailRegEx;
    let emailValid = reg.test(value);
    this.setState({
      email: value,
      emailValid
    });
  };
  onChangeFirstName = value => {
    const reg = constants.nameRegex;
    let nameValid = reg.test(value);
    this.setState({
      firstName: value,
      firstNameValid: nameValid
    });
  };
  onChangeLastName = value => {
    const reg = constants.nameRegex;
    let nameValid = reg.test(value);
    this.setState({
      lastName: value,
      lastNameValid: nameValid
    });
  };
  onChangePassword = value => {
    const reg = constants.passwordRegEx;
    let passwordValid = reg.test(value);
    this.setState({
      password: value,
      passwordValid
    });
  };
  onChangeMobile = value => {
    isValidMobile = false;
    if (value.length == 10) {
      isValidMobile = true;
    }
    this.setState({
      mobileNumber: value,
      mobileValid: isValidMobile
    });
  };
  onEndEditingFirstName = value => {
    const reg = constants.nameRegex;
    let nameValid = reg.test(this.state.firstName);
    if (!nameValid && this.state.firstName !== "") {
      this.setState({
        isNotValidFirstName: true
      });
    } else {
      this.setState({
        isNotValidFirstName: false
      });
    }
  };

  onEndEditingLastName = value => {
    const reg = constants.nameRegex;
    let nameValid = reg.test(this.state.lastName);
    if (!nameValid && this.state.lastName !== "") {
      this.setState({
        isNotValidLastName: true
      });
    } else {
      this.setState({
        isNotValidLastName: false
      });
    }
  };

  onEndEditingMobile = obj => {
    isValidMobile = false;
    if (this.state.mobileNumber.length == 10) {
      isValidMobile = true;
    }
    if (!isValidMobile && this.state.mobileNumber !== "") {
      this.setState({
        isWrongMobile: true
      });
    } else {
      this.setState({
        isWrongMobile: false
      });
    }
  };
  onEndEditingEmail = obj => {
    const reg = constants.emailRegEx;
    let emailValid = reg.test(this.state.email);
    if (!emailValid && this.state.email !== "") {
      this.setState({
        isWrongEmail: true
      });
    } else {
      this.setState({
        isWrongEmail: false
      });
    }
  };
  onEndEditingPassword = obj => {
    const reg = constants.passwordRegEx;
    let passwordValid = reg.test(this.state.password);
    if (!passwordValid && this.state.password !== "") {
      this.setState({
        isValidPassword: true
      });
    } else {
      this.setState({
        isValidPassword: false
      });
    }
  };
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  render() {
    return (
        <ScrollView>
      <View style={styles.container}>
        <Dialog
    visible={this.state.modalVisible}
    footer={
      <DialogFooter>
        <DialogButton
          text="CANCEL"
          onPress={() => {
            this.setModalVisible(false);
          }}
        />
        <DialogButton
          text="Agree"
          onPress={() => {
            this.setModalVisible(false);
            this.props.navigation.push('LoginScreen')
          }}
        />
      </DialogFooter>
    }
  >
    <DialogContent>
     <Text > {constants.terms}</Text>
    </DialogContent>
  </Dialog>
        <Text style={styles.headerText}>Registration Screen</Text>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.inputs}
            placeholder="First Name"
            keyboardType="email-address"
            underlineColorAndroid="transparent"
            onEndEditing={this.onEndEditingFirstName}
            onChangeText={this.onChangeFirstName}
          />
        </View>
        {this.state.isNotValidFirstName ? (
          <Text style={styles.errorContainer}>Invalid First Name</Text>
        ) : null}

        <View style={styles.inputContainer}>
          <TextInput
            style={styles.inputs}
            placeholder="Last Name"
            keyboardType="email-address"
            underlineColorAndroid="transparent"
            onEndEditing={this.onEndEditingLastName}
            onChangeText={this.onChangeLastName}
          />
        </View>
        {this.state.isNotValidLastName ? (
          <Text style={styles.errorContainer}>Invalid Last Name</Text>
        ) : null}

        <View style={styles.inputContainer}>
          <TextInput
            style={styles.inputs}
            placeholder="Mobile Number"
            keyboardType="phone-pad"
            onEndEditing={this.onEndEditingMobile}
            underlineColorAndroid="transparent"
            onChangeText={this.onChangeMobile}
          />
        </View>
        {this.state.isWrongMobile ? (
          <Text style={styles.errorContainer}>Invalid Mobile Number</Text>
        ) : null}
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.inputs}
            placeholder="Email"
            keyboardType="email-address"
            underlineColorAndroid="transparent"
            onEndEditing={this.onEndEditingEmail}
            onChangeText={this.onChangeEmail}
          />
        </View>
        {this.state.isWrongEmail ? (
          <Text style={styles.errorContainer}>Invalid Email ID</Text>
        ) : null}

        <View style={styles.inputContainer}>
          <TextInput
            style={styles.inputs}
            placeholder="Password"
            secureTextEntry={true}
            underlineColorAndroid="transparent"
            onEndEditing={this.onEndEditingPassword}
            onChangeText={this.onChangePassword}
          />
        </View>
        {this.state.isValidPassword ? (
          <Text style={styles.errorContainer}>Invalid Password</Text>
        ) : null}

        <TouchableHighlight
          disabled={!(this.state.emailValid && this.state.passwordValid & this.state.mobileValid && this.state.firstNameValid && this.state.lastNameValid)}
          style={[
            styles.buttonContainer,this.state.emailValid && this.state.passwordValid && this.state.mobileValid && this.state.firstNameValid && this.state.lastNameValid ? styles.signupButton
              : styles.signupButton_grey
          ]}
          onPress={() => this.onClickListener("sign_up")}
        >
          <Text style={styles.signInBtnText}>Sign Up</Text>
        </TouchableHighlight>
      </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  model_container:{
    width: 250, 
    height: 100,
    backgroundColor: '#FFF',
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  inputContainer: {
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    marginTop: 8
  },
  errorContainer: {
    width: "100%",
    paddingLeft: 16,
    paddingRight: 16,
    color: "red",
    flexDirection: "row",
    textAlign: "right",
    justifyContent: "flex-end"
  },

  inputs: {
    height: 45,
    marginLeft: 16,
    marginRight: 16,
    backgroundColor: "#D3D3D3",
    paddingLeft: 8,
    borderWidth: 0.2,
    // Set border Radius.
    borderRadius: 5,
    flex: 1
  },
  inputIcon: {
    width: 30,
    height: 30,
    marginLeft: 15,
    justifyContent: "center"
  },
  buttonContainer: {
    height: 45,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
    width: 250,
    borderRadius: 5
  },
  headerText:{
    fontSize :24,
    color:'black',
    marginBottom: 10,
    marginTop:50,
  },
  signupButton: {
    backgroundColor: "#FF4DFF"
  },
  signupButton_grey: {
    backgroundColor: "#d3d3d3"
  },
  signUpText: {
    color: "white"
  }
});
